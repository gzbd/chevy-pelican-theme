(function($) {
  $(document).ready(function() {
    var $sidebarEl = $('#sidebar'),
        $menuButtonEl = $('#menu-button');

    $menuButtonEl.on('click', function() {
      $sidebarEl.toggleClass('shown');
    });
  });
})(jQuery)
