Pelican blog engine theme.

Before use please install npm modules, by running:
```
$ npm install
```
and compile sass files, by running:

```
$ sass scss:static/css
```
